@echo off

if "%1" == "" goto usage

mklink /J %1\src\Orchard.Web\Modules\Proligence.SignalR %~dp0
goto done

:usage
echo Usage: MapToOrchard.cmd OrchardRootDir
goto exit

:done
echo Done.

:exit